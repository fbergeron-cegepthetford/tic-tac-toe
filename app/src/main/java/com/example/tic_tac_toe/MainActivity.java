package com.example.tic_tac_toe;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    enum Joueurs {BLEU, ROUGE};

    Map<String, Button> cases = new HashMap<>();
    Joueurs courant;

    TextView tvTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initialiserVue();
    }

    private void initialiserVue(){
        setContentView(R.layout.activity_main);

        courant = Joueurs.BLEU;

        tvTour = this.findViewById(R.id.tvJoueurCourant);

        cases.put("00", this.findViewById(R.id.btn00));
        cases.put("01", this.findViewById(R.id.btn01));
        cases.put("02", this.findViewById(R.id.btn02));
        cases.put("10", this.findViewById(R.id.btn10));
        cases.put("11", this.findViewById(R.id.btn11));
        cases.put("12", this.findViewById(R.id.btn12));
        cases.put("20", this.findViewById(R.id.btn20));
        cases.put("21", this.findViewById(R.id.btn21));
        cases.put("22", this.findViewById(R.id.btn22));

        for (Map.Entry<String, Button> e : cases.entrySet()){
            Button btn = e.getValue();
            btn.setOnClickListener(v -> {
                if(courant == Joueurs.BLEU) {
                    btn.setBackgroundColor(Color.BLUE);
                    courant = Joueurs.ROUGE;
                }
                else{
                    btn.setBackgroundColor(Color.RED);
                    courant = Joueurs.BLEU;
                }
                setTour();
            });
        }
    }

    private void setTour(){
        if(courant == Joueurs.BLEU) {
            tvTour.setText("Joueur bleu");
        }
        else{
            tvTour.setText("Joueur rouge");
        }
    }
}